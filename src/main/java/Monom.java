/**
 * Created by Adelin on 11-Mar-17.
 */

public class Monom implements Comparable<Monom> {
    private double coefficient;
    private int power;

    public Monom(){
    }

    public Monom(double d, int p){
        this.coefficient=d;
        this.power=p;
    }

    public double getCoefficient() {
        return coefficient;
    }
    public int getPower() {
        return power;
    }

    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Monom)) return false;

        Monom monom = (Monom) o;

        if (Double.compare(monom.coefficient, coefficient) != 0) return false;
        return power == monom.power;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(coefficient);
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + power;
        return result;
    }

    public int compareTo(Monom s) {
        if (power > s.getPower())
            return -1;

        if (power == s.getPower())
            return 0;

        return 1;
    }
}

