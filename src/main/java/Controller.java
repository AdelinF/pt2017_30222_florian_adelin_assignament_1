/**
 * Created by Adelin on 11-Mar-17.
 * In clasa controller se implementeaza toate metodele care
 * realizeaza operatiile pentru polinoame.
 */
import javax.swing.*;
import java.text.DecimalFormat;
import java.util.Collections;

public class Controller {

    /*
    *Din stingul primit de la utilizator se genereaza polinomul
    */
    public Polinom parseToPolinom(String s) {
        Polinom polinomResult = new Polinom();
        s=s.replace("^-"," "); //in cazul in care exista puteri negative explicatie mai jos
        s=s.replace("-","+-"); //pentru a putea face split dupa semnul + si fiecare monom sa pastreze semnul
        s=s.replace("^",""); //prezenta "^" nu ajuta, mai mult incurca in cazul de fata :))
        String[] monoms = s.split("\\+"); // array-ul de stringuri contine monoamele polinomului
        for(String monom: monoms){
            monom = monom.replace(" ","-"); //in cazul in care au fost gasite puteri negative X^-2 se transforma in X-2 avand in vedere si lini 17
            String[] parts = monom.split("[XxYyZz]"); //pt fiecare monom, parts (lungime maxim 2) contine coeficientul si putere lui X sau nimic
            try { //in cazul in care stringul primit nu se potriveste cerintelor
                if (parts.length == 2) { // daca prts are lungimea 2 ==> exista si coeficient si putere
                    if (parts[0].equals("")) { //daca parts[0] nu contine nimic ==> monomul arata gen X^a
                        polinomResult.addToList(new Monom(Double.parseDouble("1"), Integer.valueOf(parts[1])));
                    } else if (parts[0].equals("-")) { //daca parts[0] contine - ==> monomul arata gen -X^a
                        polinomResult.addToList(new Monom(Double.parseDouble("-1"), Integer.valueOf(parts[1])));
                    } else { // daca parts are lungimea 2 si nu se potriveste in cazurile de mai sus ==> monomul arata gen b^a, nu mai conteaza ce semn au a,b
                        polinomResult.addToList(new Monom(Double.parseDouble(parts[0]), Integer.valueOf(parts[1])));
                    }
                } else if (parts.length == 1) { //daca parts are lungimea 1 inseamna ca nu exista putere sau puterea ii 1
                    if (!(parts[0].equals(""))) {
                        if (parts[0].equals("-")) { //daca parts contine doar - ==> monomul arata gen -X
                            polinomResult.addToList(new Monom(Double.parseDouble("-1"), 1));
                        } else if (parts[0].contains("-")) {
                            if (monom.length() >= 2 && !(monom.contains(""))) { //daca se intra pe ramura aceasta monomul are puterea 0
                                polinomResult.addToList(new Monom(Double.parseDouble(parts[0]), 0));
                            } else {
                                polinomResult.addToList(new Monom(Double.parseDouble(parts[0]), 1)); // monom de forma -aX
                            }
                        } else {
                            if (monom.contains("X")) { //monom de forma aX
                                polinomResult.addToList(new Monom(Double.parseDouble(parts[0]), 1));
                            } else { //monom cu puterea 0
                                polinomResult.addToList(new Monom(Double.parseDouble(parts[0]), 0));
                            }
                        }
                    }
                } else { // daca parts are lungimea 0 avem monomul X simplu
                    polinomResult.addToList(new Monom(Double.parseDouble("1"), 1));
                }
            }
            catch (Exception e){
                //daca metoda parseToPolinom esueaza
                JOptionPane.showMessageDialog(null, "Something went wrong, try entering a correct polynomial");
                polinomResult.clearList();
                return null;
            }
        }
        return polinomResult;
    }
    /*
    Din polinom se genereaza un string pentru a putea fi afisat la ecran
    */
    public String toString(Polinom p){
        Collections.sort(p.getPolinom());
        simplify(p);
        simplify(p);
        String result = "0";
        for(Monom monom: p.getPolinom()){
            if((monom.getCoefficient()==(double)1)&&(monom.getPower()==1)){ //daca monomul ii X=> adugam +X la string
                result = result.concat("+X");
            }else if((monom.getCoefficient()==(double)-1)&&(monom.getPower()==1)){ //daca monomul ii -X=> adugam -X la string
                result = result.concat("-X");
            }else if((monom.getCoefficient()==(double)-1)&&(monom.getPower()==-1)){//daca monomul ii -X^-1=> adugam -X^-1 la string
                result = result.concat("-X^-1");
            }else if((monom.getCoefficient()==(double)1)&&(monom.getPower()==-1)){//daca monomul ii -X=> adugam X^-1 la string
                result = result.concat("+X^-1");
            }else if(monom.getPower()!=0){
                if(monom.getCoefficient()<0) { // coeficient<0 se aduga - sau alt numar ngatic
                    if(monom.getCoefficient()==(double)-1){
                        result = result.concat("-");
                    }else {
                        result = result.concat(String.valueOf(monom.getCoefficient()));
                    }
                }else{//coeficient>0 se aduga + si un numar pozitiv sau doar + daca coeficient =1
                    if(monom.getCoefficient()!=1) {
                        result = result.concat("+");
                        result = result.concat(String.valueOf(monom.getCoefficient()));
                    }else{
                        result = result.concat("+");
                    }
                }
                if(monom.getPower()==1){ //daca puterea=1 se adauga X altfel X^ si oricare ar fi puterea
                    result = result.concat("X");
                }else {
                    result = result.concat("X^");
                    result = result.concat(String.valueOf(monom.getPower()));
                }
            }
            if((monom.getPower()==0)){ //daca e doar termen liber se verifica pozitivitatea si se adauga in string
                if(monom.getCoefficient()<0) {
                    result = result.concat(String.valueOf(monom.getCoefficient()));
                }else{
                    result = result.concat("+");
                    result = result.concat(String.valueOf(monom.getCoefficient()));
                }
            }
        }
        //se sterge 0 de la inceputul stringului si eventualul + daca exista
        if(result.length()!=1) {
            if (String.valueOf(result.charAt(1)).equals("+")) {
                result = result.substring(2);
            }
            if(String.valueOf(result.charAt(0)).equals("0")&&!String.valueOf(result.charAt(1)).equals(".")){
                result = result.substring(1);
            }
        }
        return result;
    }
    /*
    * Metoda aduna termeni care au puteri egale eliminand astfel duplicatele in raport la puteri
    * */
    private Polinom simplify(Polinom p1){
        Collections.sort(p1.getPolinom());
        for(int i=0; i<p1.getPolinom().size()-1; i++){
            if(p1.getMonom(i).getPower()==p1.getMonom(i+1).getPower()){
                p1.getMonom(i).setCoefficient(p1.getMonom(i).getCoefficient()+p1.getMonom(i+1).getCoefficient());
                p1.removeFromList(p1.getMonom(i+1));
            }
            if(p1.getMonom(i).getCoefficient()==0){
                p1.removeFromList(p1.getMonom(i));
            }
        }
        return p1;
    }
    /*
    * Se adauga toate monoamele intr-o singura lista dupa care se foloseste metoda simplify()
    * */
    public Polinom add(Polinom p1, Polinom p2){
        for(Monom monom: p2.getPolinom()){
            p1.addToList(monom);
        }
        simplify(p1);
        return p1;
    }
    /*
    * Se adauga toate monoamele intr-o singura lista coeficientul scazatorului fiind inmultit cu -1 dupa care se foloseste metoda simplify()
    * */
    public Polinom substract(Polinom p1, Polinom p2){
        for(Monom monom: p2.getPolinom()){
            monom.setCoefficient(monom.getCoefficient()*(-1));
            p1.addToList(monom);
        }
        simplify(p1);
        return p1;
    }
    /*
    * Efectueaza operatia de derivare
    * */
    public Polinom derive(Polinom p1){
        for (Monom monom: p1.getPolinom()){
            if(monom.getPower()!=0){
                monom.setCoefficient(monom.getCoefficient()*monom.getPower());
                monom.setPower(monom.getPower()-1);
            }else{
                monom.setCoefficient(0);
            }
        }
        simplify(p1);
        return p1;
    }
    /*
    * Efectueaza operatia de integrare
    * */
    public Polinom integrate(Polinom p1){
        for (Monom monom: p1.getPolinom()){
            if(monom.getPower()!=-1) {
                monom.setPower(monom.getPower() + 1);
                monom.setCoefficient(monom.getCoefficient() / monom.getPower());
                monom.setCoefficient(Double.parseDouble(new DecimalFormat("##.##").format(monom.getCoefficient())));
            }
        }
        //simplify(p1);
        return p1;
    }
    /*
    * Efectueaza operatia de inmultire
    * */
    public Polinom multiply(Polinom p1, Polinom p2){
        Polinom resultPolinom = new Polinom();
        for(Monom firstMonom: p1.getPolinom()){
            for(Monom monom: p2.getPolinom()){
                resultPolinom.addToList(new Monom(firstMonom.getCoefficient()*monom.getCoefficient(),firstMonom.getPower()+monom.getPower()));
            }
        }
        simplify(resultPolinom);
        return resultPolinom;
    }
    /*
    * Efectueaza operatia de impartire
    * */
    public Polinom[] divide(Polinom f, Polinom g){
        simplify(f);
        simplify(g);
        Polinom result = new Polinom();
        if(f.equals(g)){
            Polinom[] finalResult = new Polinom[2];
            finalResult[1] = result;
            finalResult[0] = new Polinom();
            finalResult[0].addToList(new Monom(1.0,0));
            return finalResult;
        }

        while(f.getMaxPower()>=g.getMaxPower()){
            Monom monom = new Monom();
            monom.setCoefficient(f.getMonomMaxDegree().getCoefficient()/g.getMonomMaxDegree().getCoefficient());
            monom.setPower(f.getMaxPower()-g.getMaxPower());
            result.addToList(new Monom(f.getMonomMaxDegree().getCoefficient()/g.getMonomMaxDegree().getCoefficient(),f.getMaxPower()-g.getMaxPower()));
            Polinom aux = new Polinom();
            aux.addToList(monom);
            aux = multiply(aux,g);
            aux = substract(f,aux);
        }
        Polinom[] finalResult = new Polinom[2];
        finalResult[0] = result;
        finalResult[1] = f;
        return finalResult;
    }
}