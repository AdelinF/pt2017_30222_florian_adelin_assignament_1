/**
 * Created by Adelin on 11-Mar-17.
 */
import java.util.*;

public class Polinom {
    private List<Monom> polinom = new ArrayList<Monom>();

    public int getMaxPower(){
        Collections.sort(polinom);
        return polinom.get(0).getPower();
    }
    public Monom getMonomMaxDegree(){
        Collections.sort(polinom);
        return this.polinom.get(0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Polinom)) return false;

        Polinom polinom1 = (Polinom) o;

        return polinom != null ? polinom.equals(polinom1.polinom) : polinom1.polinom == null;
    }

    @Override
    public int hashCode() {
        return polinom != null ? polinom.hashCode() : 0;
    }

    public Monom getMonom(int i){
        return polinom.get(i);
    }

    public List<Monom> getPolinom() {
        return polinom;
    }

    public void addToList(Monom m){
        polinom.add(m);
    }
    public void removeFromList(Monom m){
        polinom.remove(m);
    }
    public void clearList(){
        this.polinom = new ArrayList<Monom>();
     }
}
