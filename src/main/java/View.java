/**
 * Created by Adelin on 11-Mar-17.
 * Clasa view creaza interfata utilizator cu toate textfield-urile
 * si butoanele aferente.
 */
import java.awt.BorderLayout;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class View extends JFrame{
    public JPanel contentPane;
    public JTextField textField1;
    public JTextField textField2;
    public JTextField resultField;
    public JButton btnDerive = new JButton("Derive");
    public JButton btnIntegrate = new JButton("Integrate");
    public JButton btnAdd = new JButton("Add");
    public JButton btnSubstract = new JButton("Substract");
    public JButton btnDivide = new JButton("Divide");
    public JButton btnMultiply = new JButton("Multiply");
    public JButton btnUseIt = new JButton("UseIt");
    public JTextField getTextField1() {
        return textField1;
    }

    public void setTextField1(JTextField textField1) {
        this.textField1 = textField1;
    }

    public JTextField getTextField2() {
        return textField2;
    }

    public void setTextField2(JTextField textField2) {
        this.textField2 = textField2;
    }

    /**
     * Create the frame.
     */
    public View() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JPanel panel = new JPanel();
        panel.setBackground(new Color(176, 251, 131));
        panel.setForeground(new Color(0, 128, 128));
        panel.setBorder(new LineBorder(Color.DARK_GRAY, 5));
        contentPane.add(panel, BorderLayout.CENTER);
        panel.setLayout(null);

        JLabel lblPolinom = new JLabel("Polinom 1 :");
        lblPolinom.setBounds(10, 25, 78, 14);
        panel.add(lblPolinom);

        textField1 = new JTextField();
        textField1.setBounds(83, 22, 331, 20);
        panel.add(textField1);
        textField1.setColumns(10);

        JLabel lblPolinom_1 = new JLabel("Polinom 2 :");
        lblPolinom_1.setBounds(10, 58, 78, 14);
        panel.add(lblPolinom_1);

        textField2 = new JTextField();
        textField2.setBounds(83, 55, 331, 20);
        panel.add(textField2);
        textField2.setColumns(10);

        btnAdd.setBounds(10, 110, 89, 23);
        panel.add(btnAdd);

        btnDerive.setBounds(226, 110, 89, 23);
        panel.add(btnDerive);

        btnIntegrate.setBounds(325, 110, 89, 23);
        panel.add(btnIntegrate);

        JLabel lblResult = new JLabel("Result:");
        lblResult.setBounds(22, 190, 46, 14);
        panel.add(lblResult);

        btnSubstract.setBounds(109, 110, 106, 23);
        panel.add(btnSubstract);

        btnDivide.setBounds(109, 144, 89, 23);
        panel.add(btnDivide);

        btnMultiply.setBounds(226, 144, 89, 23);
        panel.add(btnMultiply);

        btnUseIt.setBounds(351, 205, 63, 20);
        panel.add(btnUseIt);

        resultField = new JTextField();
        resultField.setBounds(10, 205, 331, 20);
        panel.add(resultField);
        resultField.setColumns(10);
    }
}
