/**
 * Created by Adelin on 11-Mar-17.
 * In clasa main am adaugat obiecte de tipul controller si view
 * pentru a putea adauga actiuni la butoana si pentru a putea
 * face operatiile din clasa controller.
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {

    public static void main(String[] args) throws Exception {
        View ui = new View();
        ui.setVisible(true);
        Controller controller = new Controller();

        ui.btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom p1= new Polinom();
                Polinom p2= new Polinom();
                p1 = controller.parseToPolinom(ui.textField1.getText());
                p2 = controller.parseToPolinom(ui.textField2.getText());
                ui.resultField.setText(controller.toString(controller.add(p1,p2)));
            }
        });
        ui.btnSubstract.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom p1= new Polinom();
                Polinom p2= new Polinom();
                p1 = controller.parseToPolinom(ui.textField1.getText());
                p2 = controller.parseToPolinom(ui.textField2.getText());
                ui.resultField.setText(controller.toString(controller.substract(p1,p2)));
            }
        });
        ui.btnDerive.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom p1= new Polinom();
                p1 = controller.parseToPolinom(ui.textField1.getText());
                ui.resultField.setText(controller.toString(controller.derive(p1)));
            }
        });
        ui.btnIntegrate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom p1= new Polinom();
                p1 = controller.parseToPolinom(ui.textField1.getText());
                //In cazul in care trebuie se integreaza X^-1, se inlocuieste
                //in Stringul rezultat cu derivata acestuia ln(x).
                String s = (controller.toString(controller.integrate(p1))).replace("X^-1","ln(X)");
                s = s.concat("+C");
                ui.resultField.setText(s);
            }
        });
        ui.btnDivide.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom p1 = new Polinom();
                Polinom p2 = new Polinom();
                p1 = controller.parseToPolinom(ui.textField1.getText());
                p2 = controller.parseToPolinom(ui.textField2.getText());
                Polinom [] result = controller.divide(p1,p2); // metoda divide returneaza un array de polinoame ==> 2: Catul si restul
                String s = controller.toString(result[0]);
                s = s.concat("   Reminder: ");
                s = s.concat(controller.toString(result[1]));
                ui.resultField.setText(s);
            }
        });
        ui.btnMultiply.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom p1= new Polinom();
                Polinom p2= new Polinom();
                p1 = controller.parseToPolinom(ui.textField1.getText());
                p2 = controller.parseToPolinom(ui.textField2.getText());
                ui.resultField.setText(controller.toString(controller.multiply(p1,p2)));
            }
        });
        ui.btnUseIt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ui.textField1.setText(ui.resultField.getText());
                ui.resultField.setText("");
            }
        });
    }

}