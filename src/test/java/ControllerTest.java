import static junit.framework.TestCase.assertTrue;
import org.junit.Test;

/**
 * Created by Adelin on 16-Mar-17.
 */
public class ControllerTest {
    private final Controller controller = new Controller();

    @Test
    public void add() throws Exception {
        // x^2 + x -1

        Monom term11 = new Monom(1, 2);
        Monom term12 = new Monom(1, 1);
        Monom term13 = new Monom(-1, 0);

        Polinom polynomial1 = new Polinom();
        polynomial1.addToList(term11);
        polynomial1.addToList(term12);
        polynomial1.addToList(term13);

        // -6x + x^2 + 8

        Monom term21 = new Monom(-6, 1);
        Monom term22 = new Monom(1, 2);
        Monom term23 = new Monom(8, 0);

        Polinom polynomial2 = new Polinom();
        polynomial1.addToList(term21);
        polynomial1.addToList(term22);
        polynomial1.addToList(term23);

        Polinom actual = controller.add(polynomial1, polynomial2);

        // 7 -5x + 2x^2
        Monom expectedMonom1 = new Monom(2, 2);
        Monom expectedMonom2 = new Monom(-5, 1);
        Monom expectedMonom3 = new Monom(7, 0);

        Polinom expected = new Polinom();
        expected.addToList(expectedMonom1);
        expected.addToList(expectedMonom2);
        expected.addToList(expectedMonom3);
        assertTrue(expected.equals(actual));
    }
}
